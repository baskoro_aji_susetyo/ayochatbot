# README #

Ayochatbot is example node.js app chatbot for facebook messenger. This will count your left day till your birthday.

# Official Facebook Page #

https://www.facebook.com/Ayo-Chatbot-392923874662942

* This is development facebook page, so only registered tester can use the feature, please let me now if you want to register your user to this apps
* You can use my dummy facebook user for test
# Endpoint #

*Get All Messages*
[GET]
https://cryptic-escarpment-94252.herokuapp.com/messages

*Get Messages by id*
[GET]
https://cryptic-escarpment-94252.herokuapp.com/messageid/{id}

*Delete Message by id*
[DELETE]
https://cryptic-escarpment-94252.herokuapp.com/messageid/{id}

# How to #
1. Login with dummy account
2. Go to the Facebook page https://www.facebook.com/Ayo-Chatbot-392923874662942.
3. Select button "Send Message".
4. Type anything, then send.
5. Input your firstname, birthdate, chatbot will automaticaly count day left till birthday.

# Note #
1. Date Input Format (YYYY-MM-DD)
2. This app was hosted using heroku free dyno, so the server will sleep when there is no request. please invoke with calling *Get All Messages* Endpoint to wake up the server.