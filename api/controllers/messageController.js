'use strict';
var mongoose = require('mongoose'),
Message = mongoose.model('Messages');
const request = require('request');

let PAGE_ACCESS_TOKEN = 'EAAhHKQB1HmoBAAnO0PbWcvLRaQAeBUaVBZCWLt8faVybvza29aCgwa8p4kX2cqvwkeYNtL41CAPOcf79Mv3XaB6Xv757AXapjvWqzcIHL1v6RxWSp8513kt4JtPPtdZA5siWXEKfoqmqfxfSmPKIBa86KfjBBoC2sRsgbRi2ul7lePzpBF';


let firstQuestion = 'Hi, What is your firstname ?';
let secondQuestion = 'What is your birthdate ?';
let thirdQuestion = 'Did you want to know your day left before your birthday ?'
var questionState = 1;
var birthdate = [];

exports.list_all_messages = function(req, res) {
    calculateNDateLeft();
    Message.find({}, function(err, message) {
        if (err)
        res.send(message);
        
        res.json(message);
    });
};


exports.create_a_message = function(req, res) {
    var new_message = new Message(req.body);
    new_message.save(function(err, message) {
      if (err)
        res.send(err);
      res.json(message);
    });
  };
  

exports.get_message_byid = function(req,res){
    Message.findById(req.params.messageId, function(err,message){
        if (err)
        res.send(err);
        
      res.json(message);
    });
};


exports.delete_message_byid = function(req,res){
    Message.deleteOne({
        _id: req.params.messageId
      }, function(err, task) {
        if (err)
          res.send(err);
        res.json({ message: 'Message successfully deleted' });
      });
};

exports.test_webhook = function(req,res){
    let VERIFY_TOKEN = "Ayochatbot_token"
    
    let mode = req.query['hub.mode'];
    let token = req.query['hub.verify_token'];
    let challenge = req.query['hub.challenge'];

    if (mode && token) {
        if (mode === 'subscribe' && token === VERIFY_TOKEN) {        

        res.status(200).send(challenge);
        
        } else {
        // Responds with '403 Forbidden' if verify tokens do not match
        res.sendStatus(403);      
        }
    }
}

exports.post_message_webhook = function(req,res){
    let body = req.body;

    if (body.object === 'page') {

        body.entry.forEach(function(entry) {
        
        let webhook_event = entry.messaging[0];
        let message = webhook_event["message"]["text"];
        let sender = webhook_event.sender.id;

        if (questionState == 1){
          //send first Question
          callSendAPI(sender, firstQuestion);
          questionState += 1;
        }
        else if (questionState == 2){
          //send second Question
          callSendAPI(sender, secondQuestion);
          questionState += 1;
        }
        else if (questionState == 3){
          if (isValidDate(message)){
            birthdate = message.split(/[.\-_]/);
            //send the last
            callSendAPI(sender, thirdQuestion);
            questionState += 1;
          }
          else{
            //repeat send message birthdate until format valid
            callSendAPI(sender, secondQuestion);
            questionState = 2;
          }
        }
        else if (questionState == 4){
          //send Last question
          let backMessage  = function(){
            let approved_message = ['yes','yeah', 'yup', 'nah', 'ok', 'oke', 'alright', 'sure']
            let rejected_message = ['no', 'not']
  
            var responseMessage = 'response message';
            if (approved_message.includes(message)){
              responseMessage = 'There are '+calculateNDateLeft() +' days left until your next birthday';
            }
            else if (rejected_message.includes(message)){
              responseMessage = 'Goodbye !';
            } 
            else {
              responseMessage = 'We dont understand what you are type, Good bye !';
            }
            return responseMessage;
          }
          callSendAPI(sender, backMessage());
          questionState = 1;
        }
        
        //save message to DB
        //TODO will change baskoro with id later
        var inputmessage = {userid:""+sender, message:""+message}
        var new_message = new Message(inputmessage);
        new_message.save(function(err, new_message) {
          if (err)
          res.send(err);
        });
      });
  
     
      // Returns a '200 OK' response to all requests
      res.status(200).send('EVENT_RECEIVED');
    } else {
      // Returns a '404 Not Found' if event is not from a page subscription
      res.sendStatus(404);
    }
}

function calculateNDateLeft(){
  var today = new Date();
  var bday = new Date(today.getFullYear(),birthdate[2]-1,birthdate[1]);
  if( today.getTime() > bday.getTime()) {
      bday.setFullYear(bday.getFullYear()+1);
  }
  var diff = bday.getTime()-today.getTime();
  var days = Math.floor(diff/(1000*60*60*24));

  return days;
}

function isValidDate(dateString) {
  //return d instanceof Date && !isNaN(d);
   // First check for the pattern
   var regex_date = /^\d{4}\-\d{1,2}\-\d{1,2}$/;

   if(!regex_date.test(dateString))
   {
       return false;
   }

   // Parse the date parts to integers
   var parts   = dateString.split("-");
   var day     = parseInt(parts[2], 10);
   var month   = parseInt(parts[1], 10);
   var year    = parseInt(parts[0], 10);

   // Check the ranges of month and year
   if(year < 1000 || year > 3000 || month == 0 || month > 12)
   {
       return false;
   }

   var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

   // Adjust for leap years
   if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
   {
       monthLength[1] = 29;
   }

   // Check the range of the day
   return day > 0 && day <= monthLength[month - 1];
}


function callSendAPI(sender_psid, response) {
  // Construct the message body
  let request_body = {
    "messaging_type": "RESPONSE",
    "recipient": {
      "id": sender_psid
    },
    "message":{
      "text":response
    }
  }

  // Send the HTTP request to the Messenger Platform
  request({
    "uri": "https://graph.facebook.com/v2.6/me/messages",
    "qs": { "access_token": PAGE_ACCESS_TOKEN },
    "method": "POST",
    "json": request_body
  }, (err, res, body) => {
    if (!err) {
      console.log('message sent !')
    } else {
      console.error("Unable to send message:" + err);
    }
  }); 
}




