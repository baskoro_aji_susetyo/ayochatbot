'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var MessageSchema = new Schema({
  userid: {
    type: String,
    required: 'enter the user id of the message'
  },
  message: {
    type: String,
    required: 'enter the messages'
  },
  
  Created_date: {
    type: Date,
    default: Date.now
  },
});

module.exports = mongoose.model('Messages', MessageSchema);