module.exports = function(app) {
    var messageList = require('../controllers/messageController');
  
    app.route('/messages')
        .get(messageList.list_all_messages)
        .post(messageList.create_a_message);

    app.route('/messageid/:messageId')
        .get(messageList.get_message_byid)
        .delete(messageList.delete_message_byid);
    
    app.route('/webhook')
        .get(messageList.test_webhook)
        .post(messageList.post_message_webhook);
    
  };
  